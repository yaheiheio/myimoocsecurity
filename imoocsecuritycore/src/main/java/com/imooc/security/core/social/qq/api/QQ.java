package com.imooc.security.core.social.qq.api;

import java.io.IOException;

/**
 * 2018/2/22 0022    jl
 */
public interface QQ {
    QQUserInfo getUserInfo();
}
