package com.imooc.security.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 2018/1/3 0003    jl
 */
@ConfigurationProperties(prefix = "imooc.security")
@Data
@Component
public class SecurityProperties {
    private BrowserProperties browser = new BrowserProperties();
    private ValidateCodeProperties validateCode = new ValidateCodeProperties();
    private SocialProperties social = new SocialProperties();
}
