package com.imooc.security.core.validate.code.sms;

/**
 * 2018/2/2 0002    jl
 */
public interface SmsCodeSender {
    void send(String mobile, String code);
}
