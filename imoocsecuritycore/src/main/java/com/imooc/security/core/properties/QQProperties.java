package com.imooc.security.core.properties;

import lombok.Data;
import org.springframework.boot.autoconfigure.social.SocialProperties;

/**
 * 2018/2/22 0022    jl
 */
@Data
public class QQProperties extends SocialProperties {
    private String providerId = "qq";
}
