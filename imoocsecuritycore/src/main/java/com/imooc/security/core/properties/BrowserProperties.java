package com.imooc.security.core.properties;

import lombok.Data;

/**
 * 2018/1/3 0003    jl
 */
@Data
public class BrowserProperties {
    private String loginPage = "/imooc-signIn.html";

    private LoginType loginType = LoginType.JSON;

    private int rememberMeSeconds = 3600;

}
