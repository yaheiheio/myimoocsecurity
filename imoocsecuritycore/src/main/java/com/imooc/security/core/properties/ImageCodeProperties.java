package com.imooc.security.core.properties;

import lombok.Data;

/**
 * 2018/1/5 0005    jl
 */
@Data
public class ImageCodeProperties extends SmsCodeProperties{
    private int width = 67;
    private int height = 23;

    public ImageCodeProperties() {
        setLength(4);

    }
}
