package com.imooc.security.core.validate.code.sms;

/**
 * 2018/2/2 0002    jl
 */
public class DefaultSmsCodeSender implements SmsCodeSender {
    @Override
    public void send(String mobile, String code) {
        System.out.println("发送验证码"+code);
    }
}
