package com.imooc.security.core.properties;

import lombok.Data;

/**
 * 2018/1/5 0005    jl
 */
@Data
public class ValidateCodeProperties {
    private ImageCodeProperties imageCode = new ImageCodeProperties();
    private SmsCodeProperties smsCode = new SmsCodeProperties();

}
