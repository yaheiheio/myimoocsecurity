package com.imooc.security.core.properties;

import lombok.Data;

/**
 * 2018/2/22 0022    jl
 */
@Data
public class SocialProperties {
    private QQProperties qq = new QQProperties();
}
