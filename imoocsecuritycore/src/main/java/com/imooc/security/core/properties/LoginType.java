package com.imooc.security.core.properties;

/**
 * 2018/1/4 0004    jl
 */
public enum LoginType {
    REDIRECT,
    JSON
}
