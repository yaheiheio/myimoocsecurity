package com.imooc.security.core.validate.code;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 2018/1/4 0004    jl
 */
@Data
public class ValidateCode {
    private String code;
    private LocalDateTime expireTime;

    public boolean isExpried() {
        return LocalDateTime.now().isAfter(expireTime);
    }

    public ValidateCode(String code, int expireIn) {
        this.code = code;
        this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
    }

    public ValidateCode(String code, LocalDateTime expireTime) {
        this.code = code;
        this.expireTime = expireTime;
    }
}
