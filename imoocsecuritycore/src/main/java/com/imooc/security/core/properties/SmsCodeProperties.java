package com.imooc.security.core.properties;

import lombok.Data;

/**
 * 2018/1/5 0005    jl
 */
@Data
public class SmsCodeProperties {
    private int length = 6;
    private int expireIn = 60;
    private String url;
}
