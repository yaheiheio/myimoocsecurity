package com.imooc.security.core.validate.code;

import com.imooc.security.core.validate.code.img.ImageCode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 2018/1/5 0005    jl
 */
//@Component
public interface ValidateCodeGenerator {
    ValidateCode generate(ServletWebRequest request);
}
