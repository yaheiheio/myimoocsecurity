package com.imooc.security.core.validate.code;

import org.springframework.security.core.AuthenticationException;

/**
 * 2018/1/4 0004    jl
 */
public class ValidateCodeException extends AuthenticationException {
    private static final long serialVersionUID = 8848687408293611636L;

    public ValidateCodeException(String msg, Throwable t) {
        super(msg, t);
    }

    public ValidateCodeException(String msg) {
        super(msg);
    }
}
