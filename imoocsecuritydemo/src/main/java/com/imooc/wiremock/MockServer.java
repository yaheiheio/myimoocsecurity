package com.imooc.wiremock;

import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.removeAllMappings;

/**
 * 2018/1/3 0003    jl
 */
public class MockServer {
    public static void main(String[] args) {
        configureFor(8060);
        removeAllMappings();

    }
}
