package com.imooc.service.impl;

import com.imooc.service.HelloService;
import org.springframework.stereotype.Service;

/**
 * 2017/12/22 0022    jl
 */
@Service
public class HelloServiceImpl implements HelloService {
    @Override
    public String greet(String name) {
        System.out.println("HelloServiceImpl  -----  greeting");
        return "hello" + name;
    }
}
