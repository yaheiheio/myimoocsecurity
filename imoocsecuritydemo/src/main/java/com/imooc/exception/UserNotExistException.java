package com.imooc.exception;

/**
 * 2017/12/22 0022    jl
 */
public class UserNotExistException extends RuntimeException {
    private static final long serialVersionUID = -6112780192479692859L;

    private String id;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserNotExistException(String id) {
        super("user not exist");
        this.id = id;
    }
}
