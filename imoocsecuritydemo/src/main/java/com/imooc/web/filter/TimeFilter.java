package com.imooc.web.filter;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.util.Date;

/**
 * 2017/12/22 0022    jl
 */
//@Component
public class TimeFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("time filter init");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("time filter start");
          long start = new Date().getTime();
        filterChain.doFilter(request, response);
        System.out.println("结束时间"+(new Date().getTime()-start));
        System.out.println("time filter end");

    }

    @Override
    public void destroy() {
        System.out.println("time filter destroy");

    }
}
