package com.imooc.web.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * 2017/12/22 0022    jl
 */
//@Aspect
//@Component
public class TimeAspect {


    @Around("execution(* com.imooc.web.controller.UserController.*(..))")
    public Object handleControllerMethod(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("time aspect start");
        Object[] objects =  proceedingJoinPoint.getArgs();
        for (Object arg : objects) {
            System.out.println("arg is "+arg);

        }
//        Object[] objects = joinPoint.getArgs();
        Object object =  proceedingJoinPoint.proceed();
        System.out.println("time aspect end");
        return object;
    }
}
