package com.imooc.web.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.imooc.dto.User;
import com.imooc.dto.UserQueryConditon;
import com.imooc.exception.UserNotExistException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * 2017/12/21 0021    jl
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {

    @GetMapping("/me")
    public Object getCurrentUser(@AuthenticationPrincipal UserDetails user) {
//        return SecurityContextHolder.getContext().getAuthentication();
//        return authentication;
        return user;
    }

    @PostMapping(value = "/u")
    public User getUser(@Valid User user) {
        return user;
    }
    @PostMapping
    @JsonView(User.UserSimpleView.class)
    public User createUser(@Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().stream().forEach(error ->{
                FieldError fieldError = (FieldError) error;
                String message = fieldError.getField() + " " + fieldError.getDefaultMessage();
                System.out.println("==============="+message);});

        }
//        System.out.println(user.getId());
//        System.out.println(user.getUsername());
//        System.out.println(user.toString());
        return user;
    }

    @GetMapping
    @JsonView(User.UserSimpleView.class)
    @ApiOperation(value = "用户查询服务")
    public List<User> getUser(UserQueryConditon userQueryConditon,@PageableDefault(page = 1,size = 10,sort = "username,desc") Pageable pageable) {
        System.out.println("=======getUser开始"+ReflectionToStringBuilder.toString(userQueryConditon, ToStringStyle.MULTI_LINE_STYLE));
        System.out.println(pageable.getPageSize());
        System.out.println(pageable.getPageNumber());
        System.out.println(pageable.getSort());
        List<User> users = new ArrayList<>();
        User user = new User();
        users.add(user);
        users.add(user);
        users.add(user);
        return users;
    }
    @JsonView(User.UserDetailView.class)
    @GetMapping(value = "/{id:\\d+}")
    public User getInfo(@ApiParam("用户ID") @PathVariable String id) {
//        throw new UserNotExistException(id);
        System.out.println("进入getInfo服务");
        User user = new User();
        user.setUsername("tom");
        user.setId(1);
        user.setPassword("1234");
        return user;
    }
}
