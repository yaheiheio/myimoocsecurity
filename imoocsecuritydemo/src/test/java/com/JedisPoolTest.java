package com;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.springframework.security.crypto.codec.Base64;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.SecureRandom;

/**
 * 2018/1/18 0018    jl
 */
public class JedisPoolTest {

    static JedisPoolConfig config;
    static JedisPool jedisPool;

    static {
        // 初始化连接池配置对象
        config = new JedisPoolConfig();
        config.setMaxIdle(10);
        config.setMaxTotal(30);
        config.setMaxWaitMillis(3*1000);
        // 实例化连接池
        jedisPool=new JedisPool(config, "192.168.1.50", 6379);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // 从连接池获取Jedis连接
        Jedis jedisConn = jedisPool.getResource();
        jedisConn.del("cities");
        jedisConn.lpush("cities","北京");
        jedisConn.lpush("cities","上海");
        jedisConn.lpush("cities","广州");
        System.out.println(jedisConn.lrange("cities",0,-1));
        // 释放连接
        close(jedisConn, jedisPool);

    }

    private static void close(Jedis jedisConn,JedisPool pool){
        if(jedisConn!=null &&pool!=null){
            pool.returnResource(jedisConn);
        }
        if(pool!=null){
            jedisPool.destroy();
        }
    }
@Test
    public void generateSeriesData() {
        int seriesLength = 16;
        int tokenLength = 16;
        SecureRandom random = null;
        random = new SecureRandom();
        byte[] newSeries = new byte[4];
        random.nextBytes(newSeries);
        String a =  new String(Base64.encode(newSeries));


        byte[] newToken2 = new byte[4];
        random.nextBytes(newToken2);
        String b =  new String(Base64.encode(newToken2));


        /*
         * 如果是第一次登录，将用户名和密码作为cookie写到本地
         */

//    HttpServletRequest request = ServletActionContext.getRequest();
//    HttpServletResponse response = ServletActionContext.getResponse();
//    String userInfo = "tom";
//    Cookie cookie = new Cookie("user",userInfo);
//    cookie.setMaxAge(360*24*60);//设置一年有效期
//    cookie.setPath("/");//可在同一应用服务器内共享方法
//    response.addCookie(cookie);//放松到客户段
    //凭这个Cookie就自动登录并不安全可以在服务端使用一个Session来管理用户。
    //当第一次登录成功后，就创建一个Session，并将用户的某些信息保存在Session
//    HttpSession session = request.getSession();
//    session.setAttribute("user", userInfo);
//    session.setMaxInactiveInterval(3600*2);//2小时
//    //但是当cookie关闭后，用于保存SessionID的JSESSIONID会消失(此时cookie并没有过期) ，所以得将JSESESSION持久化
//    Cookie sessionId = new Cookie("JSESESSIONID",session.getId());
//    sessionId.setMaxAge(2*60);//设置两小时
//    sessionId.setPath("/");
//    response.addCookie(sessionId);



    //当用户第二次登陆时，检测这个cookie是否存在
//    Cookie[] cookies = request.getCookies();
//    for (Cookie cookie2 : cookies) {
//        //如果存在这个cookie进行页面跳转
//        if(cookie2.equals("user")){
//            if(session.getAttribute("user")!=null){
//                request.getRequestDispatcher("直接进入主页面的url").forward(request, response);
//                break;
//            }else{
//                //跳转到登录页面
//            }
//
//        }
//    }
    //如果使用上面的代码，即使浏览器关闭，在两小时之内，Web程序仍然可以自动登录。
    //如果我们自已加一个JSESSIONID Cookie，在第一次访问Web程序时，
    //HTTP响应头有两个JSESSIONID，但由于这两个JSESSIONID的值完全一样，没有任何影响
    //如果在响应头的Set-Cookie字段中有多个相同的Cookie，则按着path和name进行比较，如果这两个值相同，
    //则认为是同一个Cookie，最后一个出现的Cookie将覆盖前面相同的Cookie

    }

    @Test
    public void testCookie() {
        String username = "tttttome@h3c.com";
        String re = StringUtils.substringBefore(username, "@");
        StringUtils.substringBefore(username, "@");
        Cookie cookie5 = new Cookie("weij", username);
        Cookie cookie7 = new Cookie("a.b", username);
        Cookie cookie8 = new Cookie("YWFhYWFAaDJjLmNvbQ", username);
        Cookie cookie4 = new Cookie("weij@h3c", username);
        Cookie cookie6 = new Cookie("weij@h3c.com", username);
    }

}
