package com.imooc.security.browser.support;

import lombok.Data;

/**
 * 2018/1/3 0003    jl
 */
@Data
public class SimpleResponse {


    private Object content;

    public SimpleResponse(Object content) {
        this.content = content;
    }

}
